// ---------------- PONG -------------------

//  TODO :
//  Planche qui dépasse parfois du jeux (le probleme vient de la vitesse de l'intervale, il faut faire un calcul de distance entre la planche et le bord du jeux)
//  Si la partie est terminée on peut continuer à jouer
//  Ajouter un bouton pause 

function randomNumber(min, max){
    if (typeof min !== "number" || isNaN(min)){
        return
    } else {
        let random = Math.floor(Math.random() * (max-min)) + min
        // evite que la balle soit parfaitement alignée dans l'axe Y au démarrage de la partie 
        if (random === 0){
            random = 1
        }
        return random
    }
}

let ball = document.querySelector(".ball")
let scorePlayer1 = document.querySelector(".scorePlayer1")
let scorePlayer2 = document.querySelector(".scorePlayer2")
let planchePlayer1 = document.querySelector(".planchePlayer1")
let planchePlayer2 = document.querySelector(".planchePlayer2")
let winnerEcriture = document.querySelector(".winnerEcriture")
let numeroPlayerWinner = document.querySelector('.numeroPlayerWinner')
let replay = document.querySelector(".replay")
let box = document.querySelector(".box")

// caches les blocs de "victoire"
winnerEcriture.style.display = 'none'
replay.style.display = "none"

// nombre de pixels de déplacements (vitesse)
let positionX = ball.offsetLeft
positionX += 0
let positionY = ball.offsetTop
positionY +=0

let positionPlanche1 = planchePlayer1.offsetTop
positionPlanche1=0
let positionPlanche1X = planchePlayer1.offsetLeft

let positionPlanche2 = planchePlayer2.offsetTop
positionPlanche2=0
let positionPlanche2X = planchePlayer2.offsetLeft

// balle au centre
ball.style.top = 315 + "px"
ball.style.left = 491 + "px"


let compteurScorePlayer1 = 1
let compteurScorePlayer2 = 1

let compteurScoreP1 = 0
let compteurScoreP2 = 0

let limitDown = box.clientHeight - planchePlayer1.clientHeight
console.log(limitDown)

let moovePlanchePlayer = setInterval(function() {
    planchePlayer1.style.top = (planchePlayer1.offsetTop + positionPlanche1) + "px"
    planchePlayer2.style.top = planchePlayer2.offsetTop + positionPlanche2 + "px"
}, 16);

setInterval(function() {
    if  (ball.offsetLeft> 975){
        compteurScoreP1 = 1
        scorePlayer1.textContent = compteurScorePlayer1++
        positionX = -15
    } else if (ball.offsetLeft<0){
        compteurScoreP2 = 1
        scorePlayer2.textContent = compteurScorePlayer2++
        positionX = 15
    } 
    ball.style.left = ball.offsetLeft + positionX + "px"

    if  (ball.offsetTop> 625){
        positionY = -8
    } else if (ball.offsetTop<0){
        positionY = 8
    } 
    ball.style.top = ball.offsetTop + positionY + "px"

    let positionSquareAxeY = ball.offsetTop + positionY
    let positionSquareAxeX = ball.offsetLeft + positionX

    let positionPlanche1AxeY = planchePlayer1.offsetTop + positionPlanche1
    let positionPlanche2AxeY = planchePlayer2.offsetTop + positionPlanche2 

    // Lorsque la balle rebondit sur les planches
    if (positionSquareAxeY>positionPlanche1AxeY && positionSquareAxeY<positionPlanche1AxeY + 100 && positionSquareAxeX<positionPlanche1X + 17){
        positionX = randomNumber(8, 13)
        positionY = randomNumber(-8, 13)
    }
    if (positionSquareAxeY>positionPlanche2AxeY && positionSquareAxeY<positionPlanche2AxeY + 100 && positionSquareAxeX>positionPlanche2X - 17){
        positionX = randomNumber(-13, -8)
        positionY = randomNumber(-13, 8)
    }

    // balle au centre lorsqu'elle touche un des deux camps par rapport au flag compteur score
    if (compteurScoreP1 === 1){
        positionX = 0  
        positionY = 0
        compteurScoreP1--
        ball.style.top = 315 + "px"
        ball.style.left = 491 + "px"
    }
    if (compteurScoreP2 === 1){
        positionX = 0  
        positionY = 0
        compteurScoreP2--
        ball.style.top = 315 + "px"
        ball.style.left = 491 + "px"

    }

    // que faire lorsqu'un joueur gagne
    if (scorePlayer1.textContent=== "3"){
        winnerEcriture.style.display = 'flex'
        numeroPlayerWinner.textContent = "Player 1 WIN !"
        replay.style.display = "block"
    } else if (scorePlayer2.textContent==="3"){
        winnerEcriture.style.display = "flex"
        numeroPlayerWinner.textContent = "Player 2 WIN !"
        replay.style.display = "block"
    }
}, 16 );


//  ------------- TOUCHES --------------------

document.addEventListener('keypress', function(space){
    if (space.code == "Space"){
            positionX = randomNumber(-8, 8)
            positionY = randomNumber(-8, 6)
            
    }
})

document.addEventListener('keydown', function(downS){
    if (downS.code == "KeyS"){
        console.log(planchePlayer1.offsetTop)

        positionPlanche1 = 10
    }
    if (planchePlayer1.offsetTop>limitDown){
        console.log(planchePlayer1.offsetTop)
        positionPlanche1 = 0
    }
})

document.addEventListener('keydown', function(downZ){
    if (downZ.code == "KeyW"){
        positionPlanche1 = -10
        if (planchePlayer1.offsetTop<6){
            positionPlanche1 = 0
        } 
    }
})

document.addEventListener('keydown', function(downL){
    if (downL.code == "KeyL"){
        positionPlanche2 = 10
    }
    if (planchePlayer2.offsetTop>540){
        positionPlanche2 = 0
    }
})

document.addEventListener('keydown', function(downO){
    if (downO.code == "KeyO"){
        positionPlanche2 = -10
        if (planchePlayer2.offsetTop<6){
            positionPlanche2 = 0
        }
    }
})

document.addEventListener('keyup', function(relache){
    if (relache.code == "KeyS" || relache.code == "KeyW"){
        positionPlanche1 = 0
    }
    if (relache.code == "KeyO" || relache.code == "KeyL"){
        positionPlanche2 = 0
    }
})

replay.addEventListener('click', function(){
    scorePlayer2.textContent = "0"
    scorePlayer1.textContent = "0"
    compteurScorePlayer1 = 1
    compteurScorePlayer2 = 1

    winnerEcriture.style.display = 'none'
    replay.style.display = "none"

    ball.style.top = 315 + "px"
    ball.style.left = 491 + "px"
    planchePlayer1.style.top = 275 + "px"
    planchePlayer2.style.top = 275 + "px"
    positionX = 3
    positionY = 3
})






